function showPeopleInfo(e)
{
	var ol = e.parentNode;
	var li = ol.getElementsByTagName('li');
	var point = -1;
	for (var i=0; i<li.length; i++) {
		if (e == li[i]) {
			point = i;
			break;
		}
	}
	var div = ol.parentNode;
	var ul = div.getElementsByTagName('ul');
	var ul0 = ul[0];
	var info = ul0.getElementsByTagName('div');
	for (var j=0; j<info.length; j++) {
		info[j].style.display = 'none';
	}
	info[point].style.display = 'block';
	var left = (point + 1) * 230 + 15;
	if (0 < point) {
		left = left + point * 13;
		if (2 < point) {
			left = left - 717;
			var span = info[point].getElementsByTagName('span');
			span[0].style.marginLeft = 424 + 'px';
		}
	}
	document.getElementById('peopleInfo').style.display = 'block';
	ul0.style.marginLeft = left + 'px';
}

function hidePeopleInfo()
{
	document.getElementById('peopleInfo').style.display = 'none';
}

var adsLeft = 1;
var adsPlay = 1;
var adsTop = [1,1];
var adPlay = [1,1];

function scrollAd()
{
	if (1 == adsPlay) {
		var ads = document.getElementById('ads');
		var left = adsLeft * 30;
		if (1687 < left) {
			left = 0;
			adsLeft = 0;
		}
		ads.style.marginLeft = - left + 'px';
		adsLeft++;
	}
}

function scrollAds(i)
{
	if (1 == adPlay[i]) {
		var ad_left = document.getElementById('ad_left' + i);
		var top = adsTop[i] * 10;
		if (938 < top) {
			top = 0;
			adsTop[i] = 0;
		}
		ad_left.style.marginTop = - top + 'px';
		adsTop[i]++;
	}
}

function playAd(value)
{
	adsPlay = value;
}

function playAds(value,i)
{
	adPlay[i] = value;
}

setInterval("scrollAds(0);scrollAds(1);", 200);
//scrollAd();

function togglePrice(s)
{
	var price = document.getElementById('price');
	if (s) {
		price.style.display = s;
	} else if ('none' != price.style.display) {
		price.style.display = 'none';
	} else {
		price.style.display = 'block';
	}
}

function showQRCode(i)
{
	var qrcode = document.getElementById('QRCode');
	var li = qrcode.getElementsByTagName('li');
	var price = document.getElementById('price');
	var ol = price.getElementsByTagName('ol');
	var a = ol[0].getElementsByTagName('a');
	for (var j=0; j<li.length; j++) {
		li[j].style.display = 'none';
		a[j].className = '';
	}
	li[i].style.display = 'block';
	a[i].className = 'selected';
}

function showQRCodes(i)
{
	var qrcode = document.getElementById('qrcodes');
	var li = qrcode.getElementsByTagName('li');
	var price = document.getElementById('prices');
	var a = price.getElementsByTagName('a');
	for (var j=0; j<li.length; j++) {
		li[j].style.display = 'none';
		a[j].className = '';
	}
	li[i].style.display = 'block';
	a[i].className = 'selected';
}

function togglePrices(s)
{
	var price = document.getElementById('shang');
	if (s) {
		price.style.display = s;
	} else if ('none' != price.style.display) {
		price.style.display = 'none';
	} else {
		price.style.display = 'block';
	}
}