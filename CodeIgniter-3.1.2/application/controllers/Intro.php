<?php

class Intro extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Advertising_model', 'advertising');
    }
    
    public function index()
    {
        $this->_d['nav'] = $this->advertising->nav();
        $this->_d['dataqwcjrl'] = array();
        $this->load->view('live/index', $this->_d);
    }
}
