<?php

class User extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
		$this->load->model('SystemConfig_model', 'SystemConfig');
		$this->load->model('UserinfoBase_model', 'UserinfoBase');
    }
    
    /**
     * 检测手机号是否已注册
     */
    public function chkphone()
    {
        $msg = array(
            '没有传入参数',
            '手机号格式错误',
            '正确',
            '手机号已被注册',
        );
        
        $check = $this->check_phone();
        echo $msg[$check];
    }
    
    /**
     * 检测用户名是否注册
     */
    public function chkname()
    {
        $msg = array(
            '没有传入参数',
            '正确',
            '用户名已被注册',
        );
        
        $check = $this->check_name();
        echo $msg[$check];
    }
	
	/**
	 * 注册邀请人查询
	 */
	public function zc_query()
	{
		$userid = $this->UserinfoBase->getTuiguangId();//print_r($userid);exit;
		$row = $this->UserinfoBase->find($userid, 'username');//print_r($row);
		echo "$userid,{$row->username}";
	}
	
	/**
	 * 用户注册
	 */
	public function reg()
	{
		
		
		$data = array(
			'code' => 0,
			'msg' => '',
	    );
		
		// 表单验证
		if (!$this->form_validation->run()) {
			$data['msg'] = validation_errors();
		}
		
		// 手机验证码
		$isphonereg = $this->SystemConfig->fetchRowByKey('isphonereg');//print_r($isphonereg);
		if (1 == $isphonereg->confval) {
			$phonecode = $this->get_post('phonecode');
			$smscode = $this->session->userdata('smscode');
			if ($smscode != $phonecode) {
				$data['msg'] = '手机验证码错误';
			}
		}
		
		
		// 数据库入库
		$postdata = $this->input->post();
		$postdata['ctime'] = $_SERVER['REQUEST_TIME'];
		$postdata['name'] = $this->get_post('username');
		$postdata['regip'] = $this->input->ip_address();
		$postdata['shuituijiantime'] = $_SERVER['REQUEST_TIME'];
		//print_r($postdata);
		if ($this->UserinfoBase->add($postdata)) {
			$data['code'] = 1;
			$data['msg'] = $this->lang->line('reg_user_success');
			
		} else {
			$data['msg'] = $this->lang->line('reg_user_fail');
		}
		//print_r($data);
		exit(json_encode($data));
	}
}
