<?php

class Live extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Advertising_model', 'advertising');
        $this->load->model('Hangqing_model', 'hangqing');
    }
    
    public function index()
    {
        $this->hangqing->fetch_style = PDO::FETCH_NUM;
        $this->_d['nav'] = $this->advertising->nav();
        $this->_d['dataqwcjrl'] = $this->hangqing->fetchLastAll(10, 0, 'ctime,title,qz,ycz,gbz');
        $this->load->view('live/index', $this->_d);
    }
    
    /**
     * 发送验证码
     */
    public function sendsms()
    {
        $msg = array(
            '请提交手机号码',
            '请勿连续发送',
            '已发送',
            '发送失败',
        );
        $send = $this->send_sms();
        echo $msg[$send];
    }
}