<?php

class MY_Controller extends CI_Controller
{
    public $_d = array();
    
    public function __construct()
    {
        parent::__construct();
    }
    
    public function isMobile($mobilephone)
    {
        if (preg_match("/^13[0-9]{1}[0-9]{8}$|15[0-9]{1}[0-9]{8}$|18[0-9]{1}[0-9]{8}$|18[0-9]{1}[0-9]{8}$/", $mobilephone)) {
            return true;
        } else {
            return false;
        }
    }
    
    public function get_post($key, $default = null)
    {
        $value = $this->input->get_post($key);
        if (!$value) {
            $value = $default;
        }
        return $value;
    }
    
    /**
     * 检测手机号是否已注册
     */
    public function check_phone()
    {
        $this->load->model('UserinfoBase_model', 'UserinfoBase');
        
        $phone = $this->get_post('phone');
        if (!$phone) {
            return 0;
            
        } elseif (!$this->isMobile($phone)) {
            return 1;
            
        }
        
        $row = $this->UserinfoBase->fetchRowByPhone($phone, 'userid');//print_r($row);exit;
        if (!$row) {
            return 2;
        }

        return 3;
    }
    
    /**
     * 检测用户名是否注册
     */
    public function check_name()
    {
        $this->load->model('UserinfoBase_model', 'UserinfoBase');
        
        $username = $this->get_post('username');
        if (!$username) {
            return 0;
        }
        
        $row = $this->UserinfoBase->fetchRowByUsername($username, 'userid');
        if (!$row) {
            return 1;
        }
        
        return 2;
    }
    
    /**
     * 发送验证码
     */
    public function send_sms()
    {
        $this->load->model('Sms_model', 'sms');
        
        $phone = $this->get_post('phone');
        if (!$phone) {
            return 0;
        }
        
        /* 时间限制 */
        $row = $this->sms->fetchLastRow($phone, 'ctime');//print_r($last);exit;
        if ($row) {
            $diff = $_SERVER['REQUEST_TIME'] - $row->ctime;
            if (90 > $diff) {
                return 1;
            }
        }
        
        $code = mt_rand(100000, 999999);
        $content = "你的验证码是{$code}，请收到验证码后，尽快完成注册";
        if ($this->short_message_service($content, $phone)) {
            return 2;
        }
        
        return 3;
    }
    
    /**
     * 短信接口
     */
    public function short_message_service($content, $phone = null)
    {
        $this->load->model('SystemConfig_model', 'SystemConfig');
        $this->load->model('Sms_model', 'sms');
        
        $smsuser = $this->SystemConfig->fetchRowByKey('smsuser');
        $smspwd = $this->SystemConfig->fetchRowByKey('smspwd');
        
        $smsbao = $this->smsbao_sms($content, $phone, md5($smspwd->confval), $smsuser->confval);
        return $insert_id = $this->sms->addRow($content, $phone);
    }
    
    /**
     * 宝信
     */
    public function smsbao_sms($content, $mobile = null, $password = null, $username = null)
    {
        $content = mb_convert_encoding($content, 'GBK', 'UTF-8');
        $query_string = "u=$username&p=$password&m=$mobile&c=$content";
        $url = "http://api.smsbao.com/sms?$query_string";
        return file_get_contents($url);
    }
}
