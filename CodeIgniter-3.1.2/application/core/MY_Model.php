<?php

class MY_Model extends CI_Model
{
    public $fetch_style = PDO::FETCH_CLASS;
    
    public function __construct() {
        parent::__construct();
    }
	
    /**
     * ID查找用户
     */
    public function find($id, $select = null, $primary_key = null)
    {
        if (!$primary_key) {
            $primary_key = $this->tbl_key;
        }

        if ($select) {
            $this->db->select($select);
        }

        $row = $this->db->get_where($this->tbl, array($primary_key => $id), 1, 0)->row();
        return $row;
    }
	
    /**
     * 增
     */
    public function add($data)
    {
        $this->db->insert($this->tbl, $data);
        if (1 != $this->db->affected_rows()) {
            print_r(array(__FILE__, __METHOD__, __LINE__));exit;
        }
        return $this->db->insert_id();
    }
    
    /**
     * 获取多行
     */
    public function fetchAll($where = null, $order_by = null, $count = null, $offset = null, $select = null)
    {
        if ($select) {
            $this->db->select($select);
        }

        if ($order_by) {
            $this->db->order_by($order_by);
        }

        $query = $this->db->get_where($this->tbl, $where, $count, $offset);
        if (8 == $this->fetch_style) {
            return $query->result();
        } elseif (3 == $this->fetch_style) {
            return $query->result_array();
        }
    }
}
