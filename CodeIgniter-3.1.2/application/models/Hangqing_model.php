<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Hangqing
 *
 * @author Administrator
 */
class Hangqing_model extends MY_Model
{
    public $tbl = 'live_hangqing';
    public $tbl_key = 'id';
    
    public function __construct()
    {
        parent::__construct();
    }
    
    public function fetchLastAll($count = 10, $offset = 0, $select = null)
    {
        $where = array(
            'status' => 1,
        );
        $order_by = "{$this->tbl_key} DESC";
        return $this->fetchAll($where, $order_by, $count, $offset, $select);
    }
}
