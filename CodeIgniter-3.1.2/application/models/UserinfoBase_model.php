<?php

class UserinfoBase_model extends MY_Model
{
    public $tbl = 'live_userinfo_base';
	public $tbl_key = 'userid';
    
    public function __construct()
    {
        parent::__construct();
    }
    
	/**
	 * 根据手机号获取用户
	 */
    public function fetchRowByPhone($phone, $field = '*')
    {
        $sql = "SELECT $field FROM `{$this->tbl}` WHERE `phone` = '$phone' LIMIT 1";
        return $this->db->query($sql)->row();
    }
    
	/**
	 * 通过用户名获取用户
	 */
    public function fetchRowByUsername($value, $field = '*')
    {
        $value = addslashes($value);
        $sql = "SELECT $field FROM `{$this->tbl}` WHERE `username` = '$value' LIMIT 1";
        return $this->db->query($sql)->row();
    }
	
	/**
	 * 获取推广用户ID
	 */
	public function getTuiguangId($key = 'tuiguangid')
	{
		if (isset($_SESSION[$key]) && ($id = $_SESSION[$key])) {
			return $id;
		}
		
		$array = $this->getRandId('userid', $this->tbl_key, 100, 0);//print_r($array);
		$max = count($array) - 1;
		$key = mt_rand(0, $max);//echo $key;exit;
		return $array[$key]->userid;
	}
	
	/**
	 * 返回随机用户ID
	 */
	public function getRandId($select = null, $order_by = null, $limit = 100, $offset = 0)
	{
		if ($select) {
			$this->db->select($select);
		}
		
		if ($order_by) {
			$this->db->order_by($order_by);
		}
		
		$array = $this->db->get($this->tbl, $limit, $offset)->result();
		return $array;
	}
}
