<?php

class Sms_model extends MY_Model
{
    public $tbl = 'live_sms';
    
    public function  __construct()
    {
        parent::__construct();
    }
    
    public function fetchLastRow($phone, $field = '*')
    {
        $sql = "SELECT $field FROM `{$this->tbl}` WHERE `phone` = '$phone' ORDER BY ctime DESC, id DESC LIMIT 1";
        return $this->db->query($sql)->row();
    }
    
    public function addRow($content, $phone = null, $ctime = null)
    {
        if (!$ctime) {
            $ctime = $_SERVER['REQUEST_TIME'];
        }
        
        $data = array(
            'phone' => $phone,
            'content' => $content,
            'ctime' => $ctime,
        );
        
        $this->db->insert($this->tbl, $data);
        return $this->db->insert_id();
    }
}
