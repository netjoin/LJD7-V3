<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Advertising_model
 *
 * @author Administrator
 */
class Advertising_model extends MY_Model
{
    public $tbl = 'live_advertising';
    
    public function __construct()
    {
        parent::__construct();
    }
    
    public function nav()
    {
        $sql = "SELECT * FROM `{$this->tbl}` WHERE `cateid` = '166' AND `status` = '1' ORDER BY `sort` LIMIT 10";
        $query = $this->db->query($sql);
        return $query->result();
    }
}
