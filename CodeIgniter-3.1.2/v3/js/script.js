function showPeopleInfo(e)
{
	var ol = e.parentNode;
	var li = ol.getElementsByTagName('li');
	var point = -1;
	for (var i=0; i<li.length; i++) {
		if (e == li[i]) {
			point = i;
			break;
		}
	}
	var div = ol.parentNode;
	var ul = div.getElementsByTagName('ul');
	var ul0 = ul[0];
	var info = ul0.getElementsByTagName('div');
	for (var j=0; j<info.length; j++) {
		info[j].style.display = 'none';
	}
	info[point].style.display = 'block';
	var left = (point + 1) * 230 + 15;
	if (0 < point) {
		left = left + point * 13;
		if (2 < point) {
			left = left - 717;
			var span = info[point].getElementsByTagName('span');
			span[0].style.marginLeft = 424 + 'px';
		}
	}
	document.getElementById('peopleInfo').style.display = 'block';
	ul0.style.marginLeft = left + 'px';
}

function hidePeopleInfo()
{
	document.getElementById('peopleInfo').style.display = 'none';
}

var adsLeft = 1;
var adsPlay = 1;
var adsTop = [1,1];
var adPlay = [1,1];

function scrollAd()
{
	if (1 == adsPlay) {
		var ads = document.getElementById('ads');
		var left = adsLeft * 30;
		if (1687 < left) {
			left = 0;
			adsLeft = 0;
		}
		ads.style.marginLeft = - left + 'px';
		adsLeft++;
	}
}

function scrollAds(i)
{
	if (1 == adPlay[i]) {
		var ad_left = document.getElementById('ad_left' + i);
		var top = adsTop[i] * 10;
		if (938 < top) {
			top = 0;
			adsTop[i] = 0;
		}
		ad_left.style.marginTop = - top + 'px';
		adsTop[i]++;
	}
}

function playAd(value)
{
	adsPlay = value;
}

function playAds(value,i)
{
	adPlay[i] = value;
}

setInterval("scrollAds(0);scrollAds(1);", 200);
//scrollAd();

function togglePrice(s)
{
	var price = document.getElementById('price');
	if (s) {
		price.style.display = s;
	} else if ('none' != price.style.display) {
		price.style.display = 'none';
	} else {
		price.style.display = 'block';
	}
}

function showQRCode(i)
{
	var qrcode = document.getElementById('QRCode');
	var li = qrcode.getElementsByTagName('li');
	var price = document.getElementById('price');
	var ol = price.getElementsByTagName('ol');
	var a = ol[0].getElementsByTagName('a');
	for (var j=0; j<li.length; j++) {
		li[j].style.display = 'none';
		a[j].className = '';
	}
	li[i].style.display = 'block';
	a[i].className = 'selected';
}

function showQRCodes(i)
{
	var qrcode = document.getElementById('qrcodes');
	var li = qrcode.getElementsByTagName('li');
	var price = document.getElementById('prices');
	var a = price.getElementsByTagName('a');
	for (var j=0; j<li.length; j++) {
		li[j].style.display = 'none';
		a[j].className = '';
	}
	li[i].style.display = 'block';
	a[i].className = 'selected';
}

function togglePrices(s)
{
	var price = document.getElementById('shang');
	if (s) {
		price.style.display = s;
	} else if ('none' != price.style.display) {
		price.style.display = 'none';
	} else {
		price.style.display = 'block';
	}
}

var article_last_id = 0;
var xmlhttp = new XMLHttpRequest();

function ergodic(json)
{
	if (json) {
		if (json.code) {
			alert(json.msg);
			
		} else {
			var data = json.data;
			var ol = document.getElementById('news_ol');
			for (var i = 0; i<data.length; i++) {
				var row = data[i];
				//alert(row.content);
				ol.insertAdjacentHTML('afterBegin', '<li><img src="img/news-li.png"><time>' + row.title + '</time><h3>' + row.content + '</h3></li>');
			}
		}
		
	} else {
		alert('ergodic ERROR');
	}
}

function insertArticle()
{
	var ol = document.getElementById('news_ol');
	var li = document.createElement('li');
	var img = document.createTextNode('<img src="">');
	li.appendChild(img);
	//ol.insertBefore(li, ol.childNodes[0]);
	ol.insertAdjacentHTML('afterBegin', '<li><img src="img/news-li.png"><time>10:59:08</time><h3>【俄罗斯央行副行长：俄罗斯将就人民币债券展开非筹资路演】俄罗斯央行副行长Sergei Shvetsov表示，俄罗斯将在12月22-23日在上海举行会议；在非筹资路演后，会有一个筹资路演。</h3></li>');
}

function ajax()
{
	
	xmlhttp.onreadystatechange = ajax_change;
	xmlhttp.open('GET', 'http://test.urlnk.com/index.php/api/article', true);
	xmlhttp.send(null);
}

function ajax_change()
{
	if (4 == xmlhttp.readyState) {
		if (200 == xmlhttp.status) {
			ergodic(xmlhttp.responseText);
			
		} else {
			alert('Problem retrieving data:' + xmlhttp.statusText);
		}
	}
}